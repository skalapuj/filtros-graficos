section .data
    mascaraMenosSignificativosDeCadaColor: db 0x3, 0x3, 0x3, 0x0, 0x3, 0x3, 0x3, 0x0, 0x3, 0x3, 0x3, 0x0, 0x3, 0x3, 0x3, 0x0 
    cero: times 16 db 0x00
    uno: dq 0x1, 0x0
    fotoEnGris: db 0x3, 0x3, 0x3, 0x80, 0x7, 0x7, 0x7, 0x80, 0xb, 0xb, 0xb, 0x80, 0xf, 0xf, 0xf, 0x80
    unoEnCadaByte: db 0x0, 0x1, 0x1, 0x1, 0x0, 0x1, 0x1, 0x1, 0x0, 0x1, 0x1, 0x1, 0x0, 0x1, 0x1, 0x1
    transparencia: times 4 dd 0xFF000000
    resordenoLosDatos: db 0x3, 0x2, 0x1, 0x0, 0x7, 0x6, 0x5, 0x4, 0xb, 0xa, 0x9, 0x8, 0xf, 0xe, 0xd, 0xc
global Descubrir_asm
section .text
Descubrir_asm:  ;rdi(src) rsi(dst) edx(ancho) ecx(altura) r8d(rowsizesrc) r9d(rowsizedst) 
    push rbp
    mov rbp, rsp
    push rbx
    push r12
    push r13
    sub rsp, 8

    %define indicei r12
    %define indicej r13
    %define variableLocal rbx

    %define mirror xmm0
    %define fuente xmm2
    %define contador xmm1
    %define resultado xmm7

    movsxd rdx, edx ;extiendo registro con ceros
    movsxd rcx, ecx ;extiendo registro con ceros
    movsxd r8, r8d ;extiendo registro con ceros

    shr rdx, 2    
    mov indicei, 0
.loopFila:
    cmp indicei, rcx
    je .fin

    mov indicej, 0
.loopColumna:
    cmp indicej, rdx
    je .cambioFila

    ;levanto los primeros 4 pixeles
    mov rax, indicej
    imul rax, 16; en rax tengo j*16
    mov variableLocal, indicei 
    imul variableLocal, r8
    add variableLocal, rax
    movdqa fuente, [rdi + variableLocal]; levanto los pixeles del src 

    ;levanto los 4 pixeles espejados al anterior
    mov variableLocal, rcx
    sub variableLocal, indicei
    dec variableLocal
    imul variableLocal, r8; en variable local tengo (height -i -1) * rowsize
    mov rax, rdx
    dec rax
    sub rax, indicej
    imul rax, 16; en rax tengo (width -1 - j) *16
    add variableLocal, rax 
    movdqa mirror, [rdi + variableLocal]; levanto los pixeles espejados a los anteriores
    movdqa xmm1, mirror   ;levante los pixeles "casi espejados"
    pshufd mirror, xmm1, 27 ;obtengo los pixeles espejado

    psrlw mirror, 2  ; me quedo con los bits menos signficitivos de cada color que como shifteo word me queda mucha basura en los demás bits de cada byte, inlcusive ensucio la transparencia
    pand mirror, [mascaraMenosSignificativosDeCadaColor] ;limpio la basura, dejando la tranparencia en cero y solo en cada color sus bit menos significativos
    pand fuente, [mascaraMenosSignificativosDeCadaColor] ;pongo la tranparencia en cero y solo en cada color sus bit menos significativos
    pxor xmm0, fuente ;xmm4 es el mirror pero como despues del pxor ya no es más el mirror y contiene el resulado de la operacion lo llamamos xmm4
    movdqa resultado, [cero]
    movdqa contador, [cero]
    pshufb xmm0, [resordenoLosDatos]

.loopBits:
    pextrq rax, contador, 0
    cmp rax, 2
    je .desocultarPixeles

   
    movdqa xmm2, xmm0 ; pongo el resulado de xor en xmm2 para poder recuperarl despué
    psrld xmm2, contador
    pand xmm2, [unoEnCadaByte] ;me quedo con el bit 7, 6 y 5
    

    por resultado, xmm2
    pslld resultado, 1

    pslld xmm2, 8
    por resultado, xmm2
    pslld resultado, 1

    pslld xmm2, 8
    por resultado, xmm2
    pslld resultado, 1
   
    paddb contador, [uno]
    jmp .loopBits
     
.desocultarPixeles:
    pslld resultado, 1
;    pand resultado, [sacarBasura]
    pshufb resultado, [fotoEnGris]   
    paddb resultado, [transparencia] ;acomodo la tranparencia
    
    mov rax, indicej
    imul rax, 16
    mov variableLocal, indicei
    imul variableLocal, r8
    add  variableLocal, rax ; en variable local tengo j*16 + i *rowsize
    movdqa [rsi + variableLocal], resultado ;oculto estos pixeles

    inc indicej
    jmp .loopColumna
.cambioFila:
    inc indicei
    jmp .loopFila
.fin:
    add rsp, 8
    pop r13
    pop r12
    pop rbx
    pop rbp
    ret