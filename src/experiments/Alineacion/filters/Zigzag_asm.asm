section .rodata:
    ALIGN 16
    debbug1: db 0x1, 0x2, 0x3, 0xff, 0x4, 0x5, 0x6, 0xff, 0x7, 0x8, 0x9, 0xff, 0xa, 0xb, 0xc, 0xff 
    debbug2: db 0xd, 0xe, 0xf, 0xff, 0x10, 0x11, 0x12, 0xff, 0x13, 0x14, 0x15, 0xff, 0x16, 0x17, 0x18, 0xff
    sacarTransparencia: db 0x00, 0x1, 0x2, 0x80, 0x4, 0x5, 0x6, 0x80, 0x8, 0x9, 0xa, 0x80, 0xc, 0xd, 0xe, 0x80
    convertirDword: db    0x8, 0x9, 0x80, 0x80, 0xa, 0xb, 0x80, 0x80,  0xc, 0xd, 0x80, 0x80, 0xe, 0xf, 0x80, 0x80,
    inviertoDword: db 0x80, 0x80, 0x80, 0x0, 0x80, 0x80, 0x80, 0x4, 0x80, 0x80, 0x80, 0x8, 0x80, 0x80, 0x80, 0xc
    quintoSumando:  dd 0x9, 0x3, 0xd, 0x7
    dividirPorCinco: times 4 dd 5.0
    primerosColoresOrdenados: db 0x1, 0x80, 0x3, 0x80, 0x5, 0x80, 0x7, 0x80, 0x5, 0x80, 0x7,0x80, 0x9, 0x80, 0xb, 0x80
    siguientesColoresOrdenados: db 0x5, 0x80, 0x7, 0x80, 0x9, 0x80, 0xb, 0x80, 0x9, 0x80, 0xb, 0x80, 0xd, 0x80, 0xf, 0x80
    primerosDeColoriesimo: db 0x80, 0x0, 0x80, 0x4, 0x80, 0x8, 0x80, 0xc, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80
    segundosDeColoriesimo: db  0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x0, 0x80, 0x4, 0x80, 0x8, 0x80, 0xc
    cero: times 16 db 0x00
    uno: dq 0x1, 0x0
    transparencia: times 4 dd 0xFF000000
    blancos: times 16 db 0xFF
global Zigzag_asm
section .text
Zigzag_asm: ;rdi(src) rsi(dst) edx(ancho) ecx(altura) r8d(rowsizesrc) r9d(rowsizedst) 
;principio:
    push rbp
    mov rbp, rsp
    push rbx
    push r12
    push r13
    push r14


    %define indicei r12
    %define indicej r13
    %define variableLocal rbx
    %define contador r14

    %define primeros xmm0
    %define siguientes xmm1
    %define resultado xmm2
    

    movsxd rdx, edx ;extiendo registro con ceros
    movsxd rcx, ecx ;extiendo registro con ceros
    movsxd r8, r8d ;extiendo registro con ceros
 
    mov indicei, 0
.loopFila:
    mov indicej, 0
    
    lea rax, [rcx-2] ; rcx es altura
    cmp indicei, rax  ;i se mueve en filas
    je .filaBlanca

    cmp indicei, 0 ;i se mueve en filas
    je .filaBlanca

.loopColumna:
    lea rax, [rdx-2] ; rdx es ancho
    cmp indicej, rax ;j se mueve en columnas
    je .columnaBlanca
 
    cmp indicej, 0 ;j se mueve en columnas
    je .columnaBlanca 

    ;si llegue hasta aca j >= 2 y j < ancho - 2

    mov rax, indicei
    imul rax, r8 ; rax tiene i*rowsize
    mov variableLocal, indicej
    sub variableLocal, 2 
    imul variableLocal, 4 ; variableLocal tiene (j-2)*4
    add rax, variableLocal
    movdqa primeros, [rdi + rax] ; como j=2+k*4  entonces j-2 siempre esta alineado a 16\

    mov rax, indicei
    imul rax, r8 ; rax tiene i*rowsize
    mov variableLocal, indicej
    add variableLocal, 2
    imul variableLocal, 4  ; variableLocal tiene (j+2)*4
    add rax, variableLocal 
    movdqa siguientes, [rdi+rax] ; como j=2+k*4  entonces j+2 siempre esta alineado a 16

    mov variableLocal, indicei
    and variableLocal, 0x3 ; me quedo con los ultimos dos bits de 

    cmp variableLocal, 1
    je .congruenteAUno
    cmp variableLocal, 3
    je .congruenteATres

 ;si llegue hasta aca estoy en caso conguente a 2 o 0
    mov contador, 0
    movdqu resultado, [cero] ; pongo cero en el resgistro que va a tener el resultado 
.loopColor:
    cmp contador, 3 ;itero tres colores
    je .pixelesSiguientes

    movdqu xmm3, primeros
    pshufb xmm3, [primerosDeColoriesimo]   ; en xmm3 pongo, por ejemplo si el color que estoy iterando es azul [0, b0, 0, b1, 0, b2, 0, b3, 0, 0, 0, 0, 0, 0, 0, 0]  sino lo mismo pero con el color indicado

    movdqu xmm4, siguientes
    pshufb xmm4, [segundosDeColoriesimo] ; en xmm4 pongo, por ejemplo si el color que estoy iterando es azul [0, 0, 0, 0, 0, 0, 0, 0, 0, b4, 0, b5, 0, b6, 0, b7]  sino lo mismo pero con el color indicado

    por xmm3, xmm4 ; en xmm3 por ejemplo si el color que estoy iterando es azul [0, b0, 0, b1, 0, b2, 0, b3, 0, b4, 0, b5, 0, b6, 0, b7]
    
    movdqu xmm5, xmm3 
    pshufb xmm5, [quintoSumando] ; si estoy iterando el azul xmm5 me quedo con [0, 0, 0, b4, 0, 0, 0, b1, 0, 0, 0, b6, 0, 0, 0, b3]
    
    movdqu xmm4, xmm3
    pshufb xmm4, [primerosColoresOrdenados] ; ordeno los colores de manera de poder sumar las cosas horizontalmente en xmm4 pongo [0, b0, 0, b1, 0, b2, 0, b3, 0, b2, 0, b3, 0, b4, 0, b5]
    pshufb xmm3, [siguientesColoresOrdenados] ; ordeno los colores de manera de poder sumar las cosas horizontalmente en xmm4 pongo [0, b2, 0, b3, 0, b4, 0, b5, 0, b4, 0, b5, 0, b6, 0, b7]
    
    phaddsw xmm4, xmm3 ; en xmm4 obtengo [0, b0+b1, 0, b2+b3, 0, b2+b3, 0, b4+b5, 0, b2+b3, 0, b4+b5, 0, b4+b5, 0, b6+b7]

    movdqu xmm3, [cero]
    phaddsw xmm3, xmm4 ; en xmm3 obtengo [0, 0, 0, 0, 0, 0, 0, 0, 0, b0+b1+b2+b3, 0, b2+b3+b4+b5, 0, b2+b3+b4+b5, 0, b4+b5+b6+b7] 

    pshufb xmm3, [convertirDword] ; en xmm3 obtengo [0, 0, 0, b0+b1+b2+b3, 0, 0, 0, b2+b3+b4+b5, 0, 0, 0, b2+b3+b4+b5, 0, 0, 0, b4+b5+b6+b7]

    paddusw xmm3, xmm5 ;sumo el quinto sumnado y obtengo [0, 0, 0, b0+b1+b2+b3+b4, 0, 0, 0, b2+b3+b4+b5+b1, 0, 0, 0, b2+b3+b4+b5+b6, 0, 0, 0, b3+b4+b5+b6+b7]

    cvtdq2ps xmm3, xmm3 ;convierto los enteros en punto flotante
 
    divps xmm3, [dividirPorCinco] ;divido cada dword por cinco
    
    cvtps2dq xmm3, xmm3    ; convierto el punto flotante en entero 
 
    pshufb xmm3, [inviertoDword] ; invierto el dword para poder ir poniendolo en l resultado de manera ordenada

    paddd resultado, xmm3 ; guardo el color iesimo
    psrld resultado, 8 ; en resultado pongo lugar para el siguiente color
 
    psrld primeros, 8   ; cambio al color siguiente
    psrld siguientes, 8 ; cambio al color siguiente
    inc contador ; cambio al color siguiente
    jmp .loopColor
.congruenteAUno:
    movdqu resultado, primeros
    pshufb resultado, [sacarTransparencia]
    jmp .pixelesSiguientes
.congruenteATres:
    movdqu resultado, siguientes
    pshufb resultado, [sacarTransparencia]
.pixelesSiguientes:
    paddd resultado, [transparencia]
    mov rax, indicei  
    imul rax, r8 ; rax tiene i * rowsize (rowsize es la cantidad de bytes de la imagen es decir ancho * 4)
    mov variableLocal, indicej
    imul variableLocal, 4 ; variableLocal (que es rbx) tiene j * 4 (cuatro es el size del dato)
    add rax, variableLocal  ; rax tiene i* rowsize + j * 4
    movdqu [rsi+ rax], resultado ; en lo que no es borde pongo el color azul

    add indicej, 4 ; incremento j en 4 ya que ya pinte los cuatro pixeles de esta vuelta
    jmp .loopColumna

.columnaBlanca:
    ; si entre aca es porque j=0 o j = ancho-2 asi que de ambas formas al resultado le tengo que poner dos pixeles blancos
    mov rax, indicei
    imul rax, r8 ;  rax tiene i* rowsize
    mov variableLocal, indicej
    imul variableLocal, 4 ; variableLocal tiene j * 4
    add rax, variableLocal ; rax tiene i* rowsize + j * 4
    mov qword [rsi + rax], 0xFFFFFFFFFFFFFFFF ;pongo dos pixeles blancos
    cmp indicej, 0 ; si era el principio de una fila incializo j en dos y sigo la fila
    je .iniciofila
    inc indicei ; sino voy a la fila siguiente
    jmp .loopFila

.iniciofila:
    ; si era el principio de una fila incializo j en dos y sigo la fila
    mov indicej, 2
    jmp .loopColumna

.filaBlanca:
    ; si llegue aca i=0, i=1, i= alto-2 i= alto-1 siempre con j=0
    cmp indicej, rdx ;en rdx tengo el ancho de la imagen que siemrpe es multiplo de 4
    je .filaLista

    mov rax, indicei
    imul rax, r8 ; i* rowsize
    mov variableLocal, indicej
    imul variableLocal, 4 ; j * 4
    add rax, variableLocal ; rax tiene i*rowsize + j * 4
    movdqa resultado, [blancos] ; voy poniendo de cuatro pixeles blancos
    movdqa [rsi + rax], resultado ; tengo memoria alineada pues j =0  y va a ir moviendose en multiplos de 16
    add indicej, 4 ;siguiente 4 pixeles
    jmp .filaBlanca
.filaLista:
    inc indicei ;cambio de fila
    mov indicej, 0 ; restauro j
    cmp indicei, 1 ; si es uno me falta una fila mas
    je .filaBlanca
    cmp indicei, 2 ; si es 2 ya puedo empezara  recorrer lsa filas
    je .loopColumna 


    lea rax, [rcx - 1]
    cmp indicei, rax ;si todavia me falta una fila de blancos 
    je .filaBlanca
   ;si llegue aca ya termine la imagen

    pop r14
    pop r13
    pop r12
    pop rbx
    pop rbp
    ret
