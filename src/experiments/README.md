Es importante que en la carpeta src/img se tengan carpetas con una o mas imagenes donde cada carpeta tenga como nombre 'anchoxalto' 

Para obtener datos de rendimiento:

*Entrar a la carpeta del filtro que se quiera obtener datos
*Entrar a la carpeta con nombre del tamaño de imagenes que se quiera correr
*Correr ../(filtro).sh (tamaño de imagen/es)
*En los archivos filtroASM filtroC se va a ver el output de correr el filtro elegido con las imagenes que estan en la carpeta src/img/(tamaño de imagen)
*Para generar los graficos, abrir en jupyter notebook rendimientoASMvsC.ipynb y correrlo


Para los experimentos siguientes se debe modificar el archivo ocultar.sh en cada carpeta y poner el nombre de una carpeta con imagenes

Para obtener datos de Alineamiento

*Entrar a la carpeta del filtro que se quiera obtener datos
*Correr ./(filtro).sh 
*En los archivos filtroASM1 filtroASM2 se va a ver el output de correr el filtro elegido con las imagenes que estan en la carpeta src/img/(tamaño de imagen)
*Para generar los graficos, abrir en jupyter notebook AlineacionASM.ipynb y correrlo


Para obtener datos de Variables

*Entrar a la carpeta del tipo de dato que se quiera obtener .data .rodata XOR Registros
*Entrar a la carpeta del filtro que se quiera obtener datos
*Correr ./(filtro).sh 
*En los archivos filtroASM1 se va a ver el output de correr el filtro elegido con las imagenes que estan en la carpeta src/img/(tamaño de imagen)
*Para generar los graficos, abrir en jupyter notebook 'nombreDelJupyter'.ipynb y correrlo
