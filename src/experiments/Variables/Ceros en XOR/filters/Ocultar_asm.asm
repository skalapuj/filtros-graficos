section .data
    siguientesPixelesDeLaTira: db 0x8, 0x80, 0xA, 0x80, 0x9, 0x80, 0x9, 0x80, 0xC, 0x80, 0xE, 0x80, 0xD, 0x80, 0xD, 0x80
    primerosPixelesDeLaTira: db 0x0, 0x80, 0x2, 0x80, 0x1, 0x80, 0x1, 0x80, 0x4, 0x80, 0x6, 0x80, 0x5, 0x80, 0x5, 0x80
    uno: dq 0x1, 0x0
    tres: dq 0x3, 0x0
    cinco: dq 0x5, 0x0
    ocho: dq 0x8, 0x0
    unoEnCadaDword: times 4 dd 0x1
    mascaraSrc:  db 0xFC, 0xFC, 0xFC, 0x00, 0xFC, 0xFC, 0xFC, 0x00, 0xFC, 0xFC, 0xFC, 0x00, 0xFC, 0xFC, 0xFC, 0x00
    mascaraMenosSignificativosDeCadaColor: db 0x3, 0x3, 0x3, 0x0, 0x3, 0x3, 0x3, 0x0, 0x3, 0x3, 0x3, 0x0, 0x3, 0x3, 0x3, 0x0 
    transparencia: times 4 dd 0xFF000000

section .text
global Ocultar_asm
Ocultar_asm: ; rdi(src) rsi(sr2) rdx(dst) ecx(ancho) r8d(altura) r9d(rowsizesrc) rsp+8(rowsizedst) 
    push rbp
    mov rbp, rsp
    push rbx
    push r12
    push r13
    sub rsp, 8

    %define indicei r12
    %define indicej r13
    %define variableLocal rbx

    %define mascaraDeGrisesOriginal xmm6
    %define mascaraDeGrises xmm7
    
    %define contador xmm5
    %define mirror xmm4
    %define fuente xmm3
    
    movsxd rcx, ecx ;extiendo registro con ceros
    movsxd r8, r8d ;extiendo registro con ceros
    movsxd r9, r9d ;extiendo registro con ceros

    shr rcx, 2    
    mov indicei, 0

.loopFila:
    cmp indicei, r8
    je .fin

    mov indicej, 0 ;empiezo a recorrer la fila desde la columna 0
.loopColumna:
    cmp indicej, rcx
    je .cambioFila

    mov rax, indicej
    imul rax, 16
    mov variableLocal, indicei
    imul variableLocal, r9 
    add variableLocal, rax ;en variableLocal tengo j*16 + i * rowsize

    movdqa xmm0, [rsi + variableLocal] 
    pshufb xmm0, [primerosPixelesDeLaTira] ;me quedo con los primeros pixeles de la tira de modo que xmm0 tenga de los pixeles 0 y 1 los datos de esta forma 0|b0|0|r0|0|g0|0|g0 0|b1|0|r1|0|g1|0|g1

    movdqa xmm1, [rsi + variableLocal]
    pshufb xmm1, [siguientesPixelesDeLaTira] ;me quedo con los siguiente pixeles de la tira de modo que xmm0 tenga de los pixeles 0 y 1 los datos de esta forma 0|b2|0|r2|0|g2|0|g2 0|b3|0|r3|0|g3|0|g3


    phaddw xmm0, xmm1 ;sumo horizontalmente para que en cada dw obtener en la parte alta ri+ bi y en la baja 2* gi
    phaddw xmm0, xmm0 ;sumo horizontalmente consiguiente en cada w r+b+2*g de cada pixel repetido en la parte alta y baja

    psrlw xmm0, 2  ; divido por cuatro logrando que en cada w contenga la escala de gris de un pixel correspondiente r+b+2*g / 4
    PMOVZXWD xmm0, xmm0 ; extiendo con ceros los ultimos para que ahora cada dw que contenía r+b+2*g / 4 se corresponda con su pixel
    ;pshufb xmm0, [imagenAOcultarEnGris] ; genero la imagen en gris
    movdqa mascaraDeGrisesOriginal, xmm0 ;conservo el resultado 
    pxor mascaraDeGrises, mascaraDeGrises ; inicializo el registro que contendra  para cada pixel su escala de gris distrubida de esta forma en cada dw estara | 0 |los bit de  r+b+2*g / 4 correpondientes al azul | 0 | los bits de  r+b+2*g / 4 correpondientes al verde | 0 |los bits de  r+b+2*g / 4 correpondientes al rojo | 0 | 0 | donde "los bit de  r+b+2*g / 4 correpondientes al x" se acomodan en la parte menos significativa de ese byte
    movdqa contador, [tres] ; son tres colores

.loopSeparoColor:
    
    pextrq rax, contador, 0
    cmp rax, 0
    je .ocultarPixeles


    movdqa xmm1, mascaraDeGrisesOriginal
    
    movdqa xmm4, [cinco]
    psubb xmm4, contador
    psrld xmm1, xmm4 ;obtengo el bit 1 del color correspondiente al contador en el bit menos significativo del dw de xmm1
    pand xmm1, [unoEnCadaDword];me quedo solo con ese biy en cada dw
    pslld xmm1, 1; lo acomodo de forma que este en el bit 1 de la dw correspondiente de xmm1

    movdqa xmm3, mascaraDeGrisesOriginal

    movdqa xmm4, [ocho]
    psubb xmm4, contador
    psrld xmm3, xmm4;obtengo el bit 0 del color correspondiente al contador en el bit menos significativo del dw de xmm1

    pand xmm3, [unoEnCadaDword];me quedo solo con ese biy en cada dw
    
    por xmm1, xmm3;junto los bits en xmm1

    paddd mascaraDeGrises, xmm1; guardo el resultado
    pslld mascaraDeGrises, 8; pongo lugar para el proximo resultado

    psubb contador, [uno] ;repito proceso con el siguiente color
    jmp .loopSeparoColor
.ocultarPixeles:
    psrld mascaraDeGrises, 8; coloco 00 en el lugar que corresponderia a la transparencia 

    mov rax, indicej
    imul rax, 16; en rax tengo j*4
    mov variableLocal, indicei 
    imul variableLocal, r9
    add variableLocal, rax
    movdqa fuente, [rdi + variableLocal]; levanto los pixeles del src que hay que alterar

    mov variableLocal, r8
    sub variableLocal, indicei
    dec variableLocal
    imul variableLocal, r9; en variable local tengo (height -i -1) * rowsize
    mov rax, rcx
    dec rax
    sub rax, indicej
    imul rax, 16; en rax tengo (width -1 - j) *16
    add variableLocal, rax 
    movdqa mirror, [rdi + variableLocal]; levanto los pixeles espejados a los anteriores
    movdqa xmm2, mirror   ;levnate los pixeles "casi espejados"
    pshufd mirror, xmm2, 27 ;obtengo los pixeles espejado

    psrlw mirror, 2  ; me quedo con los bits menos signficitivos de cada color que como shifteo word me queda mucha basura en los demás bits de cada byte, inlcusive ensucio la transparencia
    pand mirror, [mascaraMenosSignificativosDeCadaColor] ;limpio la basura, dejando la tranparencia en cero y solo en cada color sus bit menos significativos
    pxor mirror, mascaraDeGrises
    
    pand fuente, [mascaraSrc]  ;me quedo con la informacion de la foto que va a contener la que voy a ocultar
    paddb fuente, mirror ;oculto la foto
    paddb fuente, [transparencia] ;acomodo la tranparencia

    mov rax, indicej
    imul rax, 16
    mov variableLocal, indicei
    imul variableLocal, r9
    add  variableLocal, rax ; en variable local tengo j*16 + i *rowsize
    movdqa [rdx + variableLocal], fuente ;oculto estos pixeles


    inc indicej
    jmp .loopColumna

.cambioFila:
    inc indicei
    jmp .loopFila

.fin:
    add rsp, 8
    pop r13
    pop r12
    pop rbx
    pop rbp
    ret
