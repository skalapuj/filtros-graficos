#!/bin/bash

for img in ../../../Descubrir/$1/*.bmp; do
	../../../../build/tp2 Zigzag -i asm -t 20 $img >> ZigzagASM
	../../../../build/tp2 Zigzag -i c -t 20 $img >> ZigzagC
done

