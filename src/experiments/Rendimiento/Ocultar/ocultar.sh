#!/bin/bash

for img1 in ../../../../img/$1/*.bmp; do
	for img2 in ../../../../img/$1/*.bmp; do
		../../../../build/tp2 Ocultar -i asm -t 20 $img1 $img2 >> OcultarASM
		../../../../build/tp2 Ocultar -i c -t 20 $img1 $img2 >> OcultarC
	done
done

